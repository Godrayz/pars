<html>
<head>
    <style type="text/css">
        table {
            border: 1px solid #000000;
        }

        .black {
            border-bottom: 1px solid #000000;
        }

        .brown {
            border-bottom: 1px solid #996633;
        }

    </style>
</head>
<body>
<?php
phpinfo();
ini_set('log_errors' , 1);
ini_set("max_execution_time", "100000");
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

/**
 * Формирует массив ссылок найденных на странице
 * @param string $pageContent
 * @return array
 */

function cutLinks(string $pageContent): array
{
    $result = [];
    $pieces = explode("<a", $pageContent);

    foreach ($pieces as $piece) {
        $hrefPiece = explode("href", $piece);
        if (count($hrefPiece) == 1) {
            continue;
        }
        $hrefPiece = $hrefPiece[1];

        $k = strpos($hrefPiece, '"');
        $j = strpos($hrefPiece, '"', $k + 1);
        $link = substr($hrefPiece, $k + 1, ($j - 1) - $k);

        if (strlen($link) !== 0 && $link[0] == '#') {
            continue;
        }

        $link = explode('#', $link)[0];

        $parsedLink = parse_url($link);

        if (false === $parsedLink) {
            continue;
        }

        $schema = 'none';
        if (isset($parsedLink['scheme'])) {
            $schema = $parsedLink['scheme'];
        }

        if (!in_array($schema, ['http', 'https', 'none'])) {
            continue;
        }

        $result[] = $link;
    }

    return $result;
}

/**
 * Скачивает страницу из интернета и находит на ней все ссылки
 * @param string $url - адрес страницы
 * @param $currentLvl
 * @return array массив ссылок
 */
function parsePage(string $url, $currentLvl): array
{

    $result = [];

    $urlInfo = parse_url($url);
    if (false === $urlInfo || !isset($urlInfo['host'])) {
        return $result;
    }
    $domain = $urlInfo['host'];
    $scheme = $urlInfo['scheme'] ?? 'http';
    $pageContent = file_get_contents($url);

    if (false === $pageContent) {
        return $result;
    }

    $links = cutLinks($pageContent);

    foreach ($links as $link) {

        $linkInfo = parse_url($link);
        if (!isset($linkInfo['host'])) {
            $link = "{$scheme}://{$domain}{$link}";
            $result[$link] = $currentLvl;
        } else {
            if ($domain === $linkInfo['host']) {
                $result[$link] = $currentLvl;
            }
        }
    }

    return $result;
}


function addUniqueItemsToArray(array $items, array $result): array
{
    foreach ($items as $key => $value) {
        if (!array_key_exists($key, $result)) {
            $resultArray[$key] = $value;
        }
    }
    if (isset($resultArray)) {
        return $resultArray;
    }

    return $result;
}

/**
 * Выдирает из массива ссылок на странице дубликаты и якори
 * @param string $url адрес исходной страницы
 * @param $requiredLvl
 * @return array
 */

function parseSite(string $url, $requiredLvl): array
{
    $keeper = [];
    $result[$url] = 0;
    $currentLvl = 0;
    $links[$url] = $currentLvl;

    while ($currentLvl < $requiredLvl) {
        $currentLvl++;
        foreach ($links as $link => $item) {
            $keeper = parsePage($link, $currentLvl);
            $keeper = addUniqueItemsToArray($keeper, $result);
        }
        $links = $keeper;
        $result = array_merge($result, $links);
    }

    return $result;
}

function createList($array)
{
    foreach ($array as $link => $item) {
        echo
        "<ul>
    <li> <a href='$link' target='_blank'> '$link'</a> $item  </li>
         </ul>";
    }
    echo "</table>";
    return $array;
}

function createNumberedList($array):void{
    $i = 1;
    echo "<table>
<tr>
<td>Номер ссылки</td> <td>Ссылка</td><td>Уровень ссылки</td>
</tr>";
    foreach ($array as $link => $key){
        echo "<tr>
<td>$i</td>
<td><a href = $link target='_blank'>'$link'</td>
<td>$key</td> 
              </tr>";
        $i++;
    }
    echo "</table>";

}

function createTable($array)
{
    $i = 0;
    $color = ['brown', 'black'];

    echo " <table>
  <tr> <td class='$color[$i]'> Ссылки </td> <td class = '$color[$i]'> Глубина уровня </td> </tr>";
    $i++;
    foreach ($array as $link => $key) {
        $j = $i % 2;
        echo "<tr>  <td class='$color[$j]'>  <a href='$link' target= _blank > $link </a> </td> <td class = '$color[$j]'> $key </td> </tr> ";
        $i++;
    }
    echo '</table> ';
    return $array;
}


//echo "Parsed: " . count(parseSite("https://folkstrategies.com", 0)) . " pages\n";
echo '<table> <ul>';
$a = parseSite('https://glavfinans.ru', 1);
createTable($a);
echo "<br>";
createList($a);
echo "<br>";
createNumberedList($a);
?>
</body>
</html>
