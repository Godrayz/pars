<?php
/**
 * @param string $picName имя картинки
 * @param string $path путь к картинке
 * @param int $width ширина к которой нужно привести
 * string $picExtension тип файла
 * array $originalSize массив в [0] которого лежит изначальная ширина, а в [1] изначальная высота
 * int $height высота картинки на выходе
 * float $percentageDifference значение процентной разницы, для поддержания пропорций картинки
 * resource $oldImg копия изначальной картинки, будет нужна для создания новой
 * resource $newImg чёрный квадрат с размерами новой картинки, нужен для создания оной
 *
 */

function createDuplicate(string $picName, string $path, int $width)
{
    $picExtension = pathinfo($picName, PATHINFO_EXTENSION);
    $originalSize = getimagesize("$path/$picName");
    if ($originalSize[0] < $width) {
        return;
    }
    $percentageDifference = $width / $originalSize[0] * 100;
    $height = $originalSize[1] / 100 * $percentageDifference;
    if ($picExtension == 'jpg') {
        echo 'created jpg';
        $oldImg = imagecreatefromjpeg("$path/$picName");
    } else {
        echo 'created png';
        $oldImg = imagecreatefrompng("$path/$picName");
    }
    $newImg = imagecreatetruecolor($width, $height);
    imagecopyresampled($newImg, $oldImg, 0, 0, 0, 0, $width, $height, $originalSize[0], $originalSize[1]);
    imgCreate($newImg, $path, $picExtension);
}

function imgCreate($imgResource, $path, $extension)
{
    if ($extension == 'jpg') {
        imagejpeg($imgResource, "$path/duplicate.$extension");
        echo 'displayed jpg';
        return;
    }
    imagepng($imgResource, "$path/duplicate.$extension");
    imagepng($imgResource);
    echo 'displayed png';
    return;
}

createDuplicate('mountains.jpg', 'downloaded', 800);