<?php
//function httpCheck(string $picName)
//{
//    if ($picName[0] == '/') {
//        $picName = 'https:' . $picName;
//    }
//    return $picName;
//}
//
//function cutPageSource(string $address)
//{
//    $pageContent = file_get_contents($address);
//    $splitContent = explode('<img src="', $pageContent);
//    $i = 1;
//    $result = [];
//    while ($i < count($splitContent)) {
//        $firstLinkPart = stripos($splitContent[$i], '"');
//        $secondLinkPart = substr($splitContent[$i], 0, $firstLinkPart);
//        if ($secondLinkPart == '') {
//            $i++;
//            continue;
//        }
//        $secondLinkPart = httpCheck($secondLinkPart);
//        $result[] = $secondLinkPart;
//        $i++;
//    }
//    return $result;
//}
//
//function downloadPics($picsArray): void
//{
//    foreach ($picsArray as $pic) {
//        $content = file_get_contents($pic);
//        $picName = parse_url($pic);
//        $picCutStart = strrpos($picName["path"], "/");
//        $cuttedPic = substr($picName["path"], $picCutStart, strlen($picName["path"]) - $picCutStart);
//        $file = fopen("downloaded/" . $cuttedPic, "w+");
//        fwrite($file, $content);
//    }
//}
//
//function showFileNames(string $path): void
//{
//    $i = 0;
//    $folderPictures = scandir($path);
//    echo "<ul>";
//    foreach ($folderPictures as $picture) {
//        if ($picture[0] == '.') {
//            continue;
//        }
//        echo "<li> $picture </li>";
//        $i++;
//        if ($i == 10) {
//            break;
//        }
//    }
//    echo "</ul>";
//}
//
//function showKartinochki(string $path)
//{
//    $duplicateNumber = 0;
//    $i = 0;
//    $picturesList = scandir($path);
//    foreach ($picturesList as $picture) {
//        if ($picture[0] == '.') {
//            $i++;
//            continue;
//        }
//        $picExtension = pathinfo($picture, PATHINFO_EXTENSION);
//        if ($picExtension == 'svg') {
//            $i++;
//            continue;
//        }
//        $picSize = getimagesize("$path/$picture");
//        if ($picSize[0] > '800') {
//            $newWidth = '800';
//            $newHeight = $picSize[1] / 100 * 31.25;
//            if ($picExtension == 'jpg') {
//                $newPic = imagecreatefromjpeg("downloaded/$picturesList[$i]");
//            } else {
//                $newPic = imagecreatefrompng("downloaded/$picturesList[$i]");
//            }
//
//            $duplicate = imagecreatetruecolor($newWidth, $newHeight);
//            imagecopyresampled($duplicate, $newPic, 0, 0, 0, 0, $newWidth, $newHeight, $picSize[0], $picSize[1]);
//            imagejpeg($duplicate, "$path/duplicate $duplicateNumber", 50);
//            $duplicateNumber++;
//        }
//        if (isset($newWidth) != 0 && $newWidth > 0) {
//            echo "<img src='$path$picture' width='$newWidth' height= '$newHeight' alt='pic'>";
//        } else {
//            echo "<img src='$path$picture' width='$picSize[0]' height='$picSize[1]' alt='pic'>";
//        }
//        echo '<br>';
//        echo "$path$picturesList[$i]";
//        $i++;
//        $newWidth = 0;
//    }
//}

function createDuplicate(string $picName, string $path, int $requiredSize)
{
    $picPath = "$path/$picName";
    $picExtension = pathinfo($picName, PATHINFO_EXTENSION);
    if (false == exif_imagetype($picPath)) {
        throw new Exception('Указан неверный тип/путь файла');
    }
    $originalSize = getimagesize($picPath);
    $width = $originalSize[0];
    $height = $originalSize[1];
    if ($requiredSize <= 0) {
        throw new Exception('Неверный размер');
    }
    if ('jpg' == $picExtension) {
        echo 'created jpg ';
        $oldImg = imagecreatefromjpeg($picPath);
    } else {
        echo 'created png ';
        $oldImg = imagecreatefrompng($picPath);
    }

    $newWidth = $requiredSize * min($width / $height, 1);
    $newHeight = $requiredSize * min($height / $width, 1);
    if (($width > $requiredSize && $height > $requiredSize) || ($width < $requiredSize && $height < $requiredSize)) {
        $newWidth = min($width, $requiredSize);
        $newHeight = min($height, $requiredSize);
    }
    $newImg = imagecreatetruecolor($newWidth, $newHeight);
    imagecopyresampled($newImg, $oldImg, 0, 0, 0, 0, $width, $height, $newWidth, $newHeight);
    imgCreate($picName, $newImg, $path, $picExtension);
}

function imgCreate(string $picName, $imgResource, string $path, string $extension)
{
    $extensionPosition = strripos($picName, '.');
    $reworkedName = substr($picName, 0, $extensionPosition);
    $reworkedName = $reworkedName . '_duplicate';
    if ($extension == 'jpg') {
        imagejpeg($imgResource, "$path/$reworkedName.$extension");
        echo 'displayed jpg';
        return;
    }
    imagepng($imgResource, "$path/$reworkedName.$extension");
    echo 'displayed png';
    return;
}


$a = 'https://www.foleon.com/blog/5-sites-for-free-stock-photos/';
echo '<pre>';
//$a = cutPageSource($a);
//downloadPics($a);
//showFileNames('downloaded/');
//showKartinochki('downloaded/');
createDublicate('cowboy.jpg', 'downloaded', 500);
echo '<pre>';
